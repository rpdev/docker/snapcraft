# Snapcraft Docker Image

This project builds a Snapcraft docker image based on
[these instructions.][snapcraft-docker]


[snapcraft-docker]: https://snapcraft.io/docs/snapcraft-docker-images

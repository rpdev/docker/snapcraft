#!/bin/sh
cat Dockerfile | sed -e "s#RUN curl -L \$(curl -H 'X-Ubuntu-Series: 16' 'https://api.snapcraft.io/api/v1/snaps/details/snapcraft.* --output snapcraft.snap#RUN curl -L https://api.snapcraft.io/api/v1/snaps/download/vMTKRaLjnOJQetI78HjntT37VuoyssFE_6751.snap --output snapcraft.snap#g" > Dockerfile.new
rm Dockerfile
mv Dockerfile.new Dockerfile
